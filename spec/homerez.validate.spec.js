'use strict';
var should = require('chai').should(),
    moment = require('moment'),
    rewire = require('rewire'),
    HE = require('homerez-errors'),
    HV = rewire('../src/homerez/validate');

describe('utility functions', function() {
  describe('_getByDotKey', function() {
    it('should get the value for a sub-structure', function() {
      // given
      var obj = {
        guest: {
          personal: {
            email: 'joe@banana.com'
          }
        }
      };
      var dotKey = 'guest.personal.email';

      // when
      var value = HV.__get__('_getByDotKey')(obj, dotKey);

      // then
      value.should.equal('joe@banana.com');
    });

    it('should get undefined for a sub-structure when the value does not exist', function() {
      // given
      var obj = {
        guest: {
          personal: {
            email: 'joe@banana.com'
          }
        }
      };
      var dotKeys = [
        'guest.email',
        'guest.personal.email.address',
        'guest.business.email'
      ];
      var _getByDotKey = HV.__get__('_getByDotKey');

      // when
      var values = dotKeys.map(function(dotKey) {
        return _getByDotKey(obj, dotKey);
      });

      // then
      values.forEach(function(value) {
        should.equal(value, undefined);
      });
    });
  });


  describe('_addToObjectByDotKey', function() {
    var testData = [
      {
        name: 'simple value one level deep',
        input: {},
        dotKey: 'test',
        value: 'banana',
        expectedResult: {
          test: 'banana'
        }
      },
      {
        name: 'simple value three levels deep',
        input: {},
        dotKey: 'guest.personal.email',
        value: 'joe@banana.com',
        expectedResult: {
          guest: {
            personal: {
              email: 'joe@banana.com',
            }
          }
        }
      },
      {
        name: 'simple value three levels deep with existing data in other field',
        input: {
          guest: {
            business: {
              email: 'joe@bananatruck.com',
            }
          }
        },
        dotKey: 'guest.personal.email',
        value: 'joe@banana.com',
        expectedResult: {
          guest: {
            business: {
              email: 'joe@bananatruck.com',
            },
            personal: {
              email: 'joe@banana.com',
            }
          }
        }
      },
      {
        name: 'simple value three levels deep with existing data in other field',
        input: {
          guest: {
            personal: {
              name: 'Joe Banana',
            }
          }
        },
        dotKey: 'guest.personal.email',
        value: 'joe@banana.com',
        expectedResult: {
          guest: {
            personal: {
              name: 'Joe Banana',
              email: 'joe@banana.com',
            }
          }
        }
      },
    ];
    testData.forEach(function(tc) {
      it('should add by dot key: ' + tc.name, function() {
        // given
        var _addToObjectByDotKey = HV.__get__('_addToObjectByDotKey');

        // when
        var result = _addToObjectByDotKey(tc.input, tc.dotKey, tc.value);

        // then
        result.should.eql(tc.expectedResult);
      });
    });
  });
});

describe('getConstraint', function() {
  var testData = [
    {
      name: 'mandatory integer > 1',
      input: HV.mandatory.integer.gt(1),
      expectedConstraint: {
        presence: true,
        numericality: { onlyInteger: true, greaterThan: 1 }
      }
    },
    {
      name: 'optional integer > 1 without default',
      input: HV.optional.integer.gt(1),
      expectedConstraint: {
        presence: false,
        numericality: { onlyInteger: true, greaterThan: 1 }
      }
    },
    {
      name: 'optional integer > 1 with default',
      input: HV.optional.integer.gt(1).default(42),
      expectedConstraint: {
        default: 42,
        presence: false,
        numericality: { onlyInteger: true, greaterThan: 1 }
      }
    },
  ];
  testData.forEach(function(tc) {
    it('should return a constraint: ' + tc.name, function() {
      var c = tc.input.getConstraint();
      c.should.eql(tc.expectedConstraint);
    });
  });
});

describe('validateOrThrow', function() {
  it('should not throw for correct arguments', function() {
    // given
    var args        = { propertyId: '00012345' };
    var constraints = { propertyId: HV.mandatory.propertyId };

    // when
    HV.validate(args, constraints);

    // then
  });

  it('should throw for missing argument', function(done) {
    // given
    var args        = { };
    var constraints = { propertyId: HV.mandatory };

    try {
      // when
      HV.validate(args, constraints);
    }
    catch(err) {
      // then
      err.message.should.contain('propertyId');
      err.message.should.not.contain('homerez.validate.spec.js');
      err.data.should.have.keys('propertyId');
      err.type.should.equal('ERROR_INVALID_ARGUMENTS');
      err.stack.should.not.be.null;
      err.stack.should.contain('homerez.validate.spec.js');
      return done();
    }

    done('should not come here');
  });

  it('should ignore extra arguments', function() {
    // given
    var args        = { propertyId: '00012345', bananas: true };
    var constraints = { propertyId: HV.mandatory };

    // when
    HV.validate(args, constraints);

    // then
  });
});

describe('Validation Constraint Creators', function() {
  var successTests = [
    // One 'old' test using the constraint generator for backwards compatibility.
    {
      name       : 'boolean: mandatory [using old, deprecated HVC style]',
      args       : { isFruit: true },
      constraints: { isFruit: HV.constraintGenerator().mandatory.boolean }
    },
    {
      name       : 'boolean: mandatory',
      args       : { isFruit: true },
      constraints: { isFruit: HV.mandatory.boolean }
    },
    {
      name       : 'integer: mandatory, > 0',
      args       : { num: 42 },
      constraints: { num: HV.mandatory.integer.gt(0) }
    },
    {
      name       : 'integer: mandatory, with negative number as argument',
      args       : { num: -42 },
      constraints: { num: HV.mandatory.integer }
    },
    {
      name       : 'integer: mandatory, odd',
      args       : { num: 43 },
      constraints: { num: HV.mandatory.odd }
    },
    {
      name       : 'integer: mandatory, even',
      args       : { num: 42  },
      constraints: { num: HV.mandatory.even }
    },
    {
      name       : 'integer: mandatory, even, > 20, <= 50',
      args       : { num: 42  },
      constraints: { num: HV.mandatory.even.gt(20).lte(50) }
    },
    {
      name       : 'number: mandatory, with positive number as argument',
      args       : { num: 42.666  },
      constraints: { num: HV.mandatory.number }
    },
    {
      name       : 'number: mandatory, with negative number as argument',
      args       : { num: -12345.6789  },
      constraints: { num: HV.mandatory.number }
    },
    {
      name       : 'number: mandatory, > 20, <= 50',
      args       : { num: 42.666  },
      constraints: { num: HV.mandatory.number.gt(20).lte(50) }
    },
    {
      name       : 'string: explicit "string" argument',
      args       : { text: 'short text' },
      constraints: { text: HV.string },
    },
    {
      name       : 'string: length equal to max length',
      args       : { text: 'short text' },
      constraints: { text: HV.mandatory.string.length.lte(10) },
    },
    {
      name       : 'string: length equal to min length',
      args       : { text: 'short text' },
      constraints: { text: HV.mandatory.string.length.gte(10) },
    },
    {
      name       : 'string: length equal to specified length (via "is")',
      args       : { text: 'short text' },
      constraints: { text: HV.mandatory.string.length.is(10) },
    },
    {
      name       : 'string: length equal to specified length (via "eq")',
      args       : { text: 'short text' },
      constraints: { text: HV.mandatory.string.length.eq(10) },
    },
    {
      name       : 'string: length between min and max length',
      args       : { text: 'short text' },
      constraints: { text: HV.mandatory.string.length.gt(5).lt(20) },
    },
    {
      name       : 'string: matching pattern',
      args       : { text: '123abcdef' },
      constraints: { text: HV.mandatory.string.format({ pattern: /.*abc.*/ }) },
    },
    {
      name       : 'date: with time',
      args       : { date: new Date()  },
      constraints: { date: HV.mandatory.datetime },
    },
    {
      name       : 'date: day without time',
      args       : { date: moment.utc().startOf('day')  },
      constraints: { date: HV.mandatory.date },
    },
    {
      name       : 'date: day without time, argument a string with 00:00:00',
      args       : { date: '2014-03-02 00:00:00' },
      constraints: { date: HV.mandatory.date },
    },
    {
      name       : 'date: day without time, argument a string without time',
      args       : { date: '2014-03-02' },
      constraints: { date: HV.mandatory.date },
    },
    {
      name       : 'email: valid gmail.com address',
      args       : { email: 'joe.banana@gmail.com' },
      constraints: { email: HV.email },
    },
    {
      name       : 'email: valid banana.me.uk address',
      args       : { email: 'joe@banana.me.uk' },
      constraints: { email: HV.email },
    },
    {
      name: 'multiple arguments',
      args: {
        text: 'short text',
        num: 10
      },
      constraints: {
        text: HV.mandatory.string.length.gt(5).lt(20),
        num: HV.integer.gt(5).lte(10)
      },
    },
    {
      name       : 'sub field: valid string',
      args       : { guest: { email: 'joe@banana.me.uk' } },
      constraints: { 'guest.email': HV.mandatory.email },
    },
  ];

  describe('validation successful tests', function() {
    successTests.forEach(function(td) {
      it('should validate ' + td.name, function() {
        HV.validate(td.args, td.constraints);
      });
    });
  });

  var optionalTests = [
    {
      name: 'optional number without default, without argument given',
      args: {
      },
      constraints: {
        num: HV.optional.integer
      },
      expectedParsedArgs: {
      }
    },
    {
      name: 'optional number without default, with argument given',
      args: {
        num: 666
      },
      constraints: {
        num: HV.optional.integer
      },
      expectedParsedArgs: {
        num: 666
      }
    },
    {
      name: 'optional number with default',
      args: {
      },
      constraints: {
        num: HV.optional.integer.default(42)
      },
      expectedParsedArgs: {
        num: 42
      }
    },
  ];

  // Will be implemented later.
  describe('validation successful tests optional arguments', function() {
    optionalTests.forEach(function(td) {
      it('should validate ' + td.name, function() {
        var parsedArgs = HV.validate(td.args, td.constraints);
        parsedArgs.should.eql(td.expectedParsedArgs);
      });
    });
  });

  var homerezTests = [
    {
      name       : 'guestId',
      args       : { guestId: '20140302' },
      constraints: { guestId: HV.guestId },
    },
    {
      name       : 'propertyId',
      args       : { propertyId: '20140302' },
      constraints: { propertyId: HV.propertyId },
    },
    {
      name       : 'inquiryId',
      args       : { inquiryId: '201403022312' },
      constraints: { inquiryId: HV.inquiryId },
    },
    {
      name       : 'new inquiryId',
      args       : { inquiryId: 'A01821035' },
      constraints: { inquiryId: HV.inquiryId },
    },
    {
      name       : 'reservationId',
      args       : { reservationId: '201403022312' },
      constraints: { reservationId: HV.reservationId },
    },
    {
      name       : 'new reservationId',
      args       : { reservationId: 'A01821035' },
      constraints: { reservationId: HV.reservationId },
    },
  ];

  describe('validation successful tests HomeRez specific validations', function() {
    homerezTests.forEach(function(td) {
      it('should validate ' + td.name, function() {
        HV.validate(td.args, td.constraints);
      });
    });
  });

  var failTests = [
    {
      name: 'incorrect property id',
      args: {
        propertyId: 'banana',
      },
      constraints: {
        propertyId: HV.mandatory.propertyId
      },
    },
    {
      name: 'incorrect guest id',
      args: {
        guestId: 'banana',
      },
      constraints: {
        guestId: HV.mandatory.guestId
      },
    },
    {
      name: 'incorrect inquiry id',
      args: {
        inquiryId: 'banana',
      },
      constraints: {
        inquiryId: HV.mandatory.inquiryId
      },
    },
    {
      name: 'boolean: mandatory, which is not a boolean',
      args: {
        isFruit: 'should be a boolean, not a string'
      },
      constraints: {
        isFruit: HV.mandatory.boolean
      }
    },
    {
      name: 'integer: mandatory, > 0, which is 0',
      args: {
        num: 0
      },
      constraints: {
        num: HV.mandatory.integer.gt(0)
      }
    },
    {
      name: 'integer: mandatory, > 10, which is 5',
      args: {
        num: 5
      },
      constraints: {
        num: HV.mandatory.integer.gt(10)
      }
    },
    {
      name: 'integer: mandatory, >= 10, which is 5',
      args: {
        num: 5
      },
      constraints: {
        num: HV.mandatory.integer.gte(10)
      }
    },
    {
      name: 'integer: mandatory, which is a string',
      args: {
        num: 'banana'
      },
      constraints: {
        num: HV.mandatory.integer
      }
    },
    {
      name: 'integer: mandatory, which is an object',
      args: {
        num: {'fruit': 'banana' }
      },
      constraints: {
        num: HV.mandatory.integer
      }
    },
    {
      name: 'number: mandatory, which is a string',
      args: {
        num: 'banana'
      },
      constraints: {
        num: HV.mandatory.number
      }
    },
    {
      name: 'string: mandatory, which is a number',
      args: {
        text: 42
      },
      constraints: {
        text: HV.mandatory.string
      }
    },
    {
      name: 'string: a text with length 10 is not greater than 10',
      args: {
        text: 'short text'
      },
      constraints: {
        text: HV.mandatory.string.length.gt(10)
      }
    },
    {
      name: 'string: a text with length 10 is not less than 10',
      args: {
        text: 'short text'
      },
      constraints: {
        text: HV.mandatory.string.length.lt(10)
      }
    },
    {
      name: 'date: start of a day, but is not a start of a day.',
      // Note that this test fails if you run it at exactly midnight.
      // Ah well ;)
      args: {
        date: moment.utc().toDate()
      },
      constraints: {
        date: HV.mandatory.date
      },
    },
  ];

  describe('validation fail tests', function() {
    failTests.forEach(function(td) {
      it('should error for ' + td.name, function() {
        var f = function() { HV.validate(td.args, td.constraints); };
        f.should.throw(HE.InvalidArgumentsError);
      });
    });
  });

  describe('return value', function() {
    it('should return the validated arguments', function() {
      // given
      var args = { text: 'test' };
      var constraints = { text: HV.mandatory.string };

      // when
      var ret = HV.validate(args, constraints);

      // then
      ret.should.eql({ text: 'test' });
    });

    it('should return the validated arguments with sub-structure', function() {
      // given
      var args = {
        guest: {
          email: 'joe@banana.com'
        }
      };

      var constraints = {
        'guest.email': HV.email
      };

      // when
      var ret = HV.validate(args, constraints);

      // then
      ret.should.eql({ guest: { email: 'joe@banana.com' } });
    });

    it('should return the validated arguments with sub-structure multiple keys', function() {
      // given
      var args = {
        guest: {
          name : 'Joe',
          email: 'joe@banana.com'
        }
      };

      var constraints = {
        'guest.name' : HV.mandatory.string.length.gt(0),
        'guest.email': HV.mandatory.email
      };

      // when
      var ret = HV.validate(args, constraints);

      // then
      ret.should.eql({ guest: { email: 'joe@banana.com', name: 'Joe' } });
    });

    it('should not contain extra arguments', function(done) {
      // given
      var args = {
        text   : 'test',
        bananas: true
      };
      var constraints = {
        text: HV.mandatory.string
      };

      // when
      var res = HV.validate(args, constraints);

      // then
      res.should.eql({ text: 'test' });

      done();
    });

    it('should contain default values of optional arguments', function() {
      // given
      var args = {
        text   : 'test',
        bananas: true
      };
      var constraints = {
        text         : HV.mandatory.string,
        'home.type'  : HV.optional.string.length.gt(0).default('apartment'),
        'home.floors': HV.optional.integer.gt(0).default(1),
      };

      // when
      var res = HV.validate(args, constraints);

      // then
      res.should.eql({
        text: 'test',
        home: {
          type: 'apartment',
          floors: 1
        }
      });
    });
  });

  describe('mongoose class', function() {
    it('should validate the mongoose class', function() {
      var fakeMongooseObject = {
        constructor: {
          modelName: 'MongooseClassName'
        }
      };

      var args = {
        someKey: fakeMongooseObject
      };

      var constraints = {
        someKey: HV.mandatory.mongoose('MongooseClassName')
      };

      HV.validate(args, constraints);
    });
  });
});

describe('validate for single arguments', function() {
  describe('successful tests', function() {
    var testData = [
      {
        name          : 'mandatory property id',
        input         : '00012345',
        constraint    : HV.mandatory.propertyId,
        expectedResult: '00012345'
      },
      {
        name          : 'mandatory int > 10',
        input         : 135,
        constraint    : HV.mandatory.integer.gt(10),
        expectedResult: 135
      },
      {
        name          : 'optional int, no default, value given',
        input         : 135,
        constraint    : HV.integer.gt(10),
        expectedResult: 135
      },
      {
        name          : 'optional int, no default, no value given',
        input         : undefined,
        constraint    : HV.integer.gt(10),
        expectedResult: undefined
      },
      {
        name          : 'optional int, with default, value given',
        input         : 135,
        constraint    : HV.integer.gt(10).default(20),
        expectedResult: 135
      },
      {
        name          : 'optional int, with default, no value given',
        input         : undefined,
        constraint    : HV.integer.gt(10).default(20),
        expectedResult: 20
      },
    ];
    testData.forEach(function(tc) {
      it('should not throw for correct argument: ' + tc.name, function() {
        var res = HV.validate(tc.input, tc.constraint);
        if (tc.expectedResult !== undefined) {
          res.should.eql(tc.expectedResult);
        }
        else {
          should.equal(res, tc.expectedResult);
        }
      });
    })
  });

  describe('failure tests', function() {
    it('should throw for missing argument', function(done) {
      try {
        HV.validate(undefined, HV.mandatory);
      }
      catch(err) {
        err.stack.should.contain('validate');
        return done();
      }

      done('should not come here');
    });

    it('should throw for incorrect argument', function(done) {
      try {
        HV.validate(2, HV.mandatory.integer.gt(10));
      }
      catch(err) {
        err.stack.should.contain('validate');
        return done();
      }

      done('should not come here');
    });
  });
});
