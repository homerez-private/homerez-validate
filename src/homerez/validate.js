'use strict;'

/**
 * homerez-validate
 *
 * This module is based on `validate.js`, with a few improvements.
 *
 * It allows argument validation as follows:
 *
 *     var HV = require('homerez-validate');
 *
 *     // Validate a bunch of arguments in an object.
 *     function getAvailability(args) {
 *       var constraints = {
 *         propertyId: HV.mandatory.propertyId,
 *         beginDate : HV.mandatory.date,
 *         endDate   : HV.mandatory.date,
 *       };
 *       var parsedArgs = HV.validate(args, constraints);
 *     }
 *
 *     // Or validate a single argument.
 *     function cancelReservation(reservationId) {
 *       HV.validate(reservationId, HV.mandatory.reservationId);
 *     }
 *
 *     // You should always use the return value of HV.validate.
 *     // Doing that, allows you to use optional arguments with default values.
 *     // For example:
 *     createProperty({ name: 'The property name' });
 *     function createProperty(args) {
 *       var constraints = {
 *         name : HV.mandatory.string.length.gt(10),
 *         type : HV.optional.string.length.gt(0).default('APARTMENT'),
 *         floor: HV.optional.integer.gte(0).default(0)
 *       };
 *       var parsedArgs = HV.validate(args, constraints);
 *
 *       // now parsedArgs is:
 *       // {
 *       //   name: 'The property name',
 *       //   type: 'APARTMENT',
 *       //   floor: 0
 *       // }
 *     }
 *     // The same for an optional argument with default
 *     function paymentRequestExtend(paymentRequestId, durationInSecondsArg) {
 *       var durationInSeconds = HV.validate(durationInSecondsArg, HV.optional.integer.default(3600));
 *     }
*
 *
 * The following constraint types are available:
 *
 * boolean
 * date
 * datetime
 * email
 * integer
 * mandatory
 * mongoose
 * string
 *
 * These are HomeRez specific:
 *
 * propertyId
 * guestId
 * inquiryId
 *
 * The following constraint modifiers are available:
 *
 * eq
 * even
 * gt
 * gte
 * length
 * lt
 * lte
 * odd
 * is
 *
 * Examples:
 *
 *     HVC().mandatory.integer.gte(10).lt(50)
 *     HVC().mandatory.string.length.eq(10)
 */

if (typeof(window) === 'undefined') {
  var _        = require('lodash');
  var moment   = require('moment');
  var HE       = require('homerez-errors');
  var validate = require('validate.js');
  var log4js   = require('log4js');
  var logger   = log4js.getLogger('homerez-validate');
}

var configuration = {
  logErrors: false
};

function logError(error) {
  if (!configuration.logErrors) {
    return;
  }
  logger.error(error.toString());
  logger.error(error.stack);
}

function configure(config) {
  if (config.hasOwnProperty('logErrors')) {
    configuration.logErrors = config.logErrors;
  }
}

validate.moment = moment;

function validateWrapper(argOrArgs, constraintOrConstraints) {
  if (typeof(argOrArgs) === 'object') {
    return validateOrThrow(argOrArgs, constraintOrConstraints);
  }
  else {
    // Here we need to make sure the return value is not
    // { argument: 'something' }
    // but just
    // 'something'.
    var res = validateOrThrow({argument: argOrArgs}, {argument: constraintOrConstraints});
    return res.argument;
  }
}

/**
 * Validates the given arguments against the given constraints, using
 * the validate.js `validate` call.
 * This function does 2 things differently from the original `validate`
 * call:
 * 1. When an argument error happens, HE.InvalidArgumentsError is thrown.
 * 2. The value of each constraint could be a `ConstraintGenerator`
 *    object. If it is, then `.$` is called on it to obtain the actual
 *    constraint object.
 * @param  {Object} args        The function's arguments.
 * @param  {Object} constraints The constraints to check against.
 * @return {nothing}            Doesn't return anything yet.
 */
function validateOrThrow(args, constraints) {
  // We need to validate the following arguments:
  // - Mandatory
  // - Optional that were passed in the args
  //
  // Then, we need to add to the return value:
  // - Optional with a default that were not passed in the args

  // Each value of the keys in 'constraints' is a ConstraintGenerator
  // Object. Therefore, we need to convert that to a constraint.
  var realConstraints = {};

  var optionalArguments = {};
  _.keys(constraints).forEach(function(key) {
    var constraint = constraints[key].getConstraint();
    if (constraint.presence) {
      // This is a mandatory constraint. Make sure that it's checked.
      realConstraints[key] = constraint;
    }
    else {
      // This is an optional constraint. See if the value is set in the args.
      var value = _getByDotKey(args, key);

      if (value) {
        // The argument was given. That means we can delete the default, if
        // present, and validate the constraint.
        realConstraints[key] = constraint;
        delete realConstraints[key].default;
      }
      else {
        // The argument was not passed. Maybe we'll need to return the default
        // value for this argument, so let's store the constraint for later use.
        optionalArguments[key] = constraint;
      }
    }
  });

  // Here we do the actual validation of the arguments.
  var res = validate(args, realConstraints);

  if (res) {
    // An error has occurred. Generate an error object and throw it.
    var messageParts = [];
    Object.keys(res).forEach(function(k) {
      var v = res[k];
      messageParts.push(k + ': ' + v);
    });
    var message = messageParts.join('; ');

    var e = new HE.InvalidArgumentsError(message, res);

    // For some reason, the stack of a homerez error is very different from
    // the one of a native error. And also completely useless. Therefore
    // we replace it by a native error stack.
    // This might cause problems when handling this error, because normally
    // the 'stack' field is hidden/magical. And we're making it a 'normal'
    // field now.
    e.stack = (new Error()).stack;

    logError(e);
    throw e;
  }

  // Everything was validated successfully.
  // Create a useful return object, with filled in default values.
  var ret = {};

  // We loop over the constraints, so we only get the fields that have
  // actually been validated. This way, any extra fields that were
  // originally in `args` are discarded.
  _.keys(realConstraints).forEach(function(dotKey) {
    var value = _getByDotKey(args, dotKey);

    ret = _addToObjectByDotKey(ret, dotKey, value);
  });

  // Add the defaults of optional fields that were not given in args.
  _.keys(optionalArguments).forEach(function(dotKey) {
    var optionalArgument = optionalArguments[dotKey];

    if (optionalArgument.hasOwnProperty('default')) {
      ret = _addToObjectByDotKey(ret, dotKey, optionalArgument.default);
    }
  });

  return ret;
}

// This function gets values from a sub-structure using "dot key" notation.
// For example:
// obj = { guest: { email: 'joe@banana.com' } };
// dotKey = 'guest.email'
// Here it will return 'joe@banana.com'.
// For an non-existing (sub)key, undefined is returned.
// dotKey = 'guest.firstName'
// dotKey = 'guest.email.address'
function _getByDotKey(obj, dotKey) {
  function index(o, i) { return o && o.hasOwnProperty(i) ? o[i] : undefined; }
  return dotKey.split('.').reduce(index, obj);
}

// This function adds a value to an object using "dot key" notation.
// For example:
// obj = _addToObjectByDotKey({test: 'ok'}, 'some.field', 'the value')
// Leads to
// obj = {
//   test: 'ok',
//   some: {
//     field: 'the value'
//   }
// }
function _addToObjectByDotKey(obj, dotKey, value) {
  var keys = dotKey.split('.');

  // Make a reference to the object, that we can modify by "entering"
  // into sub-structures.
  var retVal = obj;

  keys.forEach(function(key, index) {
    if (index === keys.length - 1) {
      // It's the last key of the chain. Set the value and done.
      retVal[key] = value;
      return;
    }

    if (!retVal.hasOwnProperty(key)) {
      // Make sure the sub-object is created if necessary.
      retVal[key] = {};
    }

    // Enter the sub-object.
    retVal = retVal[key];
  });

  return obj;
}

function ConstraintGenerator() {
  var self = this;

  // To keep track of which field is currently 'active'. This determines
  // which field things like 'eq' or 'gt' apply to.
  self.currentField = '';

  // These are the constraints that will be passed to `validate.js`.
  self.tmpConstraints = {};

  function addMagicProperty(name, codeRef) {
    Object.defineProperty(self, name, {
      get: function() {
        codeRef();
        return self;
      },
      set: function(a) {
        throw new Error('You cannot set a magic property.');
      },
    });
  }

  function setOrUpdate(field, subField, value) {
    self.currentField = field;
    if (!self.tmpConstraints[field]) {
      self.tmpConstraints[field] = {};
    }

    self.tmpConstraints[field][subField] = value;
  }

  // mandatory / optional

  addMagicProperty('mandatory', function() {
    self.tmpConstraints.presence = true;
    delete self.tmpConstraints.default;
  });

  addMagicProperty('optional', function() {
    self.tmpConstraints.presence = false;
  });

  self.default = function(value) {
    if (self.tmpConstraints.presence) {
      throw new Error('You cannot set a default for a mandatory argument.');
    }
    self.tmpConstraints.default = value;
    return self;
  };

  // Existing validate.js functionality

  addMagicProperty('date', function() {
    setOrUpdate('datetime', 'dateOnly', true);
  });

  addMagicProperty('datetime', function() {
    setOrUpdate('datetime', 'dateOnly', false);
  });

  addMagicProperty('number', function() {
    setOrUpdate('numericality', 'onlyInteger', false);
  });

  addMagicProperty('integer', function() {
    setOrUpdate('numericality', 'onlyInteger', true);
  });

  addMagicProperty('odd', function() {
    setOrUpdate('numericality', 'odd', true);
  });

  addMagicProperty('even', function() {
    setOrUpdate('numericality', 'even', true);
  });

  addMagicProperty('length', function() {
    self.currentField = 'length';
  });

  self.eq = function(value) {
    if (self.currentField === 'numericality') {
      setOrUpdate(self.currentField, 'equalTo', value);
    }
    else if (self.currentField === 'length') {
      setOrUpdate(self.currentField, 'is', value);
    }
    else {
      throw new Error('Cannot use eq() or is() here.');
    }
    return self;
  };
  self.is = self.eq;

  self.lt = function(value) {
    if (self.currentField === 'numericality') {
      setOrUpdate(self.currentField, 'lessThan', value);
    }
    else if (self.currentField === 'length') {
      setOrUpdate(self.currentField, 'maximum', value - 1);
    }
    else {
      throw new Error('Cannot use lt() here.');
    }
    return self;
  };

  self.lte = function(value) {
    if (self.currentField === 'numericality') {
      setOrUpdate(self.currentField, 'lessThanOrEqualTo', value);
    }
    else if (self.currentField === 'length') {
      setOrUpdate(self.currentField, 'maximum', value);
    }
    else {
      throw new Error('Cannot use lte() here.');
    }
    return self;
  };

  self.gt = function(value) {
    if (self.currentField === 'numericality') {
      setOrUpdate(self.currentField, 'greaterThan', value);
    }
    else if (self.currentField === 'length') {
      setOrUpdate(self.currentField, 'minimum', value + 1);
    }
    else {
      throw new Error('Cannot use gt() here.');
    }
    return self;
  };

  self.gte = function(value) {
    if (self.currentField === 'numericality') {
      setOrUpdate(self.currentField, 'greaterThanOrEqualTo', value);
    }
    else if (self.currentField === 'length') {
      setOrUpdate(self.currentField, 'minimum', value);
    }
    else {
      throw new Error('Cannot use gte() here.');
    }
    return self;
  };

  validate.validators.string = function(value, options, key, attributes) {
    if (typeof(value) !== 'string') {
      return 'string required but this is a(n) ' + typeof(value);
    }
  };

  addMagicProperty('string', function() {
    self.tmpConstraints.string = true;
  });

  validate.validators.boolean = function(value, options, key, attributes) {
    if (typeof(value) !== 'boolean') {
      return 'boolean required but this is a(n) ' + typeof(value);
    }
  };

  addMagicProperty('boolean', function() {
    self.tmpConstraints.boolean = true;
  });

  self.format = function(format) {
    self.tmpConstraints.format = format;
    return self;
  };

  addMagicProperty('email', function() {
    self.tmpConstraints.email = true;
  });

  validate.validators.mongoose = function(value, options, key, attributes) {
    if (value.constructor.modelName !== options) {
      return 'object required but this is a(n) ' + value.constructor;
    }
  };

  self.mongoose = function(className) {
    self.tmpConstraints.mongoose = className;
    return self;
  };

  // HomeRez specific fields

  addMagicProperty('propertyId', function() {
    self.tmpConstraints.format = {
      pattern: /^\d{8}$/,
      message: 'must be an 8 digit property id'
    };
  });

  addMagicProperty('reservationId', function() {
    self.tmpConstraints.format = {
      pattern: /^(\d{12}|A\d{8})$/,
      message: 'must have either 12 digits or \'A\' followed by 8 digits'
    };
  });

  addMagicProperty('guestId', function() {
    self.tmpConstraints.format = {
      pattern: /^\d{8}$/,
      message: 'must be an 8 digit guest id'
    };
  });

  addMagicProperty('inquiryId', function() {
    self.tmpConstraints.format = {
      pattern: /^(\d{12}|A\d{8})$/,
      message: 'must have either 12 digits or \'A\' followed by 8 digits'
    };
  });

  self.raw = function(key, value) {
    self.tmpConstraints[key] = value;
    return self;
  };

  self.getConstraint = function() {
    var ret = {};

    Object.keys(self.tmpConstraints).forEach(function(key) {
      ret[key] = self.tmpConstraints[key];
    });

    return ret;
  };

  return self;
}

function createConstraintGenerator() {
  return new ConstraintGenerator();
}

var exp = {
  configure: configure,

  validate: validateWrapper,

  // Still here for backwards compatibility.
  // Needs to be removed soon.
  validateOne: validateWrapper,

  // Still here for backwards compatibility.
  // Can be removed when API no longer uses HVC().
  constraintGenerator: createConstraintGenerator,
};

Object.defineProperty(exp, 'mandatory', {
  get: function() {
    return createConstraintGenerator().mandatory;
  },
  set: function() {
    throw new Error('You cannot set this.');
  },
});

Object.defineProperty(exp, 'optional', {
  get: function() {
    return createConstraintGenerator().optional;
  },
  set: function() {
    throw new Error('You cannot set this.');
  },
});

var autoOptionalKeys = [
  'boolean',
  'date',
  'datetime',
  'email',
  'integer',
  'mongoose',
  'string',

  'propertyId',
  'guestId',
  'inquiryId',
  'reservationId',
];

// Calling HV.string will do the same as HV.optional.string.
autoOptionalKeys.forEach(function(key) {
  Object.defineProperty(exp, key, {
    get: function() {
      return createConstraintGenerator().optional[key];
    },
    set: function() {
      throw new Error('You cannot set this.');
    },
  });
})

// Check if this is being run under node. If so, only then
// we can assign to module.exports.
if (typeof(window) === 'undefined') {
  module.exports = exp;
}
